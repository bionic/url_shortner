import axios from "axios"

export default {
    /**
     * The base url for API, should process .env but hardcoded for project
     * @return {string}
     */
    endpointBaseURL() {
        return 'http://localhost:3000'
    },

    /**
     * Goes off to API to get the shortenURL
     * @param original_url
     * @return {Promise<unknown>}
     */
    shortenURL(original_url) {
        return new Promise((resolve, reject) => {
            axios.post(this.endpointBaseURL() + '/api/v1/generate', {original_url: original_url})
                .then((response) => {
                    console.log(response);

                    resolve({success: true, url: response.data.url});
                })
                .catch((error) => {
                    console.log(error);

                    reject({success: false, error: error})
                })
        });
    }
}