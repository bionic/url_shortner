import Vue from 'vue'
import App from './App.vue'
import { ValidationProvider, ValidationObserver} from 'vee-validate';
import '@/assets/scss/main.scss';
import '@/assets/tailwind.css'

Vue.config.productionTip = false;

Vue.component('validation-provider', ValidationProvider);
Vue.component('validation-observer', ValidationObserver);

new Vue({
    render: h => h(App),
}).$mount('#app');
