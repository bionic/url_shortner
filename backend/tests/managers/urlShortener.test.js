import urlShortener from './../../src/managers/urlShortener';

describe('managers/urlShortener class', () => {
    test('Gives new URL', () => {
        expect(urlShortener.createShortURL('www.test.com')).toHaveLength(29);
    });

    test('Can create a slug', () => {
        expect(urlShortener.createRandomSlug(7)).toHaveLength(7);
    });
});