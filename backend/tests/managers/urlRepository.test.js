import urlRepository from '../../src/managers/urlRepository'

describe('URL Repository class', () => {
    test('Can insert URL to repository', () => {
        urlRepository.insertURL('ABCDEFG', 'www.test.com');

        expect(urlRepository.getAllURLs().length).toEqual(1);
    });

    test('Can get original URL from repository by slug', () => {
        urlRepository.insertURL('ABCDEFG', 'www.test.com');

        expect(urlRepository.getOriginalURLForSlug('ABCDEFG')).toMatch('www.test.com');
    });

    test('Get original URL from slug to be false if not found', () => {
        urlRepository.insertURL('ABCDEFG', 'www.test.com');

        expect(urlRepository.getOriginalURLForSlug('GFEDCBA')).toBeFalsy();
    });

    test('Will return false if invalid URL on validation', () => {
        expect(urlRepository.isValidInsertRequest('ABCDEFG', null)).toBeFalsy();
    });

    test('Will return true on valid URL on validation', () => {
        expect(urlRepository.isValidInsertRequest('ABCDEFG', 'www.test.com')).toBeTruthy();
    });

    test('Will prepend http protocol if not provided', () => {
        expect(urlRepository.prependProtocol('www.test.com')).toMatch('http://www.test.com');
    });

    test('Will not prepend http protocol if providing https', () => {
        expect(urlRepository.prependProtocol('https://www.test.com')).toMatch('https://www.test.com');
    });

    test('Will not prepend http protocol if providing http', () => {
        expect(urlRepository.prependProtocol('http://www.test.com')).toMatch('http://www.test.com');
    });
});