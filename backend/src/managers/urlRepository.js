import _ from "lodash";

export default {
    urls: [],

    /**
     * Insert URL into the DB (this demo it is just in array)
     * @param slug
     * @param original_url
     */
    insertURL(slug, original_url) {
        if(!this.isValidInsertRequest(slug, original_url)) return false;

        this.urls.push({slug: slug, original_url: this.prependProtocol(original_url)});
    },

    /**
     * @param slug
     * @return {boolean|*[]}
     */
    getOriginalURLForSlug(slug) {
        let repository_record = this.urls.filter((obj) => {
            return obj.slug === slug;
        })[0];

        if(!repository_record) return false;

        return repository_record.original_url;
    },

    /**
     * Checks request is valid, more validation like regex etc should be done
     * @param slug
     * @param original_url
     * @return {boolean}
     */
    isValidInsertRequest(slug, original_url){
        if(_.isNull(original_url) || _.isUndefined(original_url) || _.isNull(slug) || _.isUndefined(slug)) return false;

        return true;
    },

    /**
     * Adds http protocol if not provided on original url
     * @param original_url
     * @return {*}
     */
    prependProtocol(original_url){
        return  !/^https?:\/\//i.test(original_url) ? `http://${original_url}` : original_url;
    },

    /**
     * Returns all the URLs in the store
     * @return {[]|*[]}
     */
    getAllURLs(){
        return this.urls;
    }
}