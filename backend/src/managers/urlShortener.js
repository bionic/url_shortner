import urlRepository from "./urlRepository";

export default {
    base_url: 'http://localhost:3000',

    slug_length: 7,

    /**
     * Creates and stores the original_url URL, returns the original_url URL
     * @return {string}
     */
    createShortURL(original_url) {
        let slug = this.createRandomSlug(this.slug_length);

        let shortened_url = this.base_url + '/' + slug;

        urlRepository.insertURL(slug, original_url);

        return shortened_url;
    },

    /**
     * Creates a random string to use as slug
     * @param length
     * @return {string}
     */
    createRandomSlug(length) {
        let str = "",
            i = 0,
            min = 10,
            max = 62;
        for (; i++ < length;) {
            let r = Math.random() * (max - min) + min << 0;
            str += String.fromCharCode(r += r > 9 ? r < 36 ? 55 : 61 : 48);
        }
        return str;
    }
}