import express from "express"
import cors from "cors"
import bodyParser from "body-parser"
import urlShortener from "./src/managers/urlShortener";
import urlRepository from "./src/managers/urlRepository";

const app = express();

app.use(cors());
app.use(bodyParser.json());


app.get('/', (req, res, next) => {
    res.send("Hello, welcome to Bionic URL");
});

app.get('/failed', (req, res, next) => {
    res.send("Failed to find original URL with that slug, please try something else.");
});

app.get('/:slug', (req, res, next) => {
    let original_url = urlRepository.getOriginalURLForSlug(req.params.slug);

    if(!original_url) res.send("Failed to find original URL, please try something else.");

    res.writeHead(301, {
        'Location': original_url
    });

    res.end();
});

app.post('/api/v1/generate', (req, res) => {
    res.json({query: req.query, url: urlShortener.createShortURL(req.body.original_url)});
});

// This is useful for checking whilst dev
app.get('/list/all', (req, res) => {
    res.json(urlRepository.getAllURLs());
});

app.listen(process.env.port || 3000);

console.log('Web Server is listening at port ' + (process.env.port || 3000));